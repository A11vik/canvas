const canvas = document.getElementById('canvas');
const ctx = canvas.getContext('2d');
let sizeStorage = localStorage.getItem('size');


const getImageFromLS = (width = 128) => {
    canvas.width = width;
    canvas.height = width;
  const dataURL = localStorage.getItem('canvasMy');

  const image = new Image();
  image.src = dataURL;
  image.onload = function () {
    if (image.width === image.height) {
      ctx.drawImage(image, 0, 0, width, width);
    } else if (image.width < image.height) {
      ctx.drawImage(image, (width - (image.width * width) / image.height) / 2,
        0, (image.width * width) / image.height, width);
    } else if (image.width > image.height) {
      ctx.drawImage(image, 0, (width - (image.height * width) / image.width) / 2,
        width, (image.height * width) / image.width);
    }
  };
};

getImageFromLS(sizeStorage);

const searchImage = async () => {
  ctx.clearRect(0, 0, 512, 512);
  canvas.width = 128;
  canvas.height = 128;
  const key = '06a5845d516e34e2d9f21de3d68bad63ea0ea722d6b4b5f2351a2f9d566b6db6';
  const query = document.getElementById('search-inp').value;
  const url = `https://api.unsplash.com/photos/random?client_id=${key}&query=${query}`;


  try {
    const res = await fetch(url);
    const data = await res.json();
    const img = new Image();

    img.src = data.urls.small;

    img.onload = function () {
      if (img.width === img.height) {
        ctx.drawImage(img, 0, 0, 128, 128);
      } else if (img.width < img.height) {
        ctx.drawImage(img, (128 - (img.width * 128) / img.height) / 2,
          0, (img.width * 128) / img.height, 128);
      } else if (img.width > img.height) {
        ctx.drawImage(img, 0, (128 - (img.height * 128) / img.width) / 2,
          128, (img.height * 128) / img.width);
      }
    };
    sizeStorage = 128;
    localStorage.setItem('size', sizeStorage);
    localStorage.setItem('canvasMy', img.src);
  } catch (err) {
    console.error('error: ', err);
  }
};

document.getElementById('search-button').addEventListener('click', searchImage);

document.getElementById('size-3').addEventListener('click', () => {
  canvas.width = 512;
  canvas.height = 512;

  getImageFromLS(512);
  sizeStorage = 512;
  localStorage.setItem('size', sizeStorage);
});

document.getElementById('size-2').addEventListener('click', () => {
  canvas.width = 256;
  canvas.height = 256;

  getImageFromLS(256);
  sizeStorage = 256;
  localStorage.setItem('size', sizeStorage);
});

document.getElementById('size-1').addEventListener('click', () => {
  canvas.width = 128;
  canvas.height = 128;

  getImageFromLS(128);
  sizeStorage = 128;
  localStorage.setItem('size', sizeStorage);
});

let isMouseDown = false;
let color = 'black';

document.getElementById('tool--red').addEventListener('click', () => color = 'red');
document.getElementById('tool--blue').addEventListener('click', () => color = 'blue');

canvas.addEventListener('mousedown', function() {
    isMouseDown = true;
});

canvas.addEventListener('mouseup', function() {
    isMouseDown = false;
    ctx.beginPath();
});

ctx.lineWidth = 5 * 2;
canvas.addEventListener('mousemove', function(e) {
    if (isMouseDown) {
        let offset = 1;
        if (sizeStorage === 128) offset = 4;
        if (sizeStorage === 256) offset = 2;
        if (sizeStorage === 512) offset = 1;

        ctx.lineTo(e.offsetX / offset, e.offsetY / offset);
        ctx.strokeStyle = color;
        ctx.stroke();
        

        ctx.beginPath();
        ctx.arc(e.offsetX / offset, e.offsetY / offset, 5, 0, Math.PI * 1.5);
        ctx.fill();
        ctx.fillStyle = ctx.fillStroke = color;

        ctx.beginPath();
        ctx.moveTo(e.offsetX / offset, e.offsetY / offset);
    }
});